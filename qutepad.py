#!/usr/bin/env python3
import os, sys, random
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

App_Name = "Qutepad"
App_Name_Small = "qutepad"
App_Version = "1.1"

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.LastFile = "None"
        self.w = QWidget()
        self.w.setWindowTitle(App_Name)
        self.w.setGeometry(100,100,470,450)
        # toolbar
        self.newButt = QPushButton(self.w)
        self.newButt.setText("New")
        self.newButt.setIcon(QIcon('icons/new.png'))
        self.newButt.move(0,0)
        self.newButt.resize(100,20)
        self.newButt.clicked.connect(self.newButt_click)
        self.savButt = QPushButton(self.w)
        self.savButt.setText("Save")
        self.savButt.move(100,0)
        self.savButt.resize(100,20)
        self.savButt.setIcon(QIcon('icons/floppy.png'))
        self.savButt.clicked.connect(self.saveButt_click)
        self.loadButt = QPushButton(self.w)
        self.loadButt.setText("Open")
        self.loadButt.move(200,0)
        self.loadButt.resize(100,20)
        self.loadButt.clicked.connect(self.openButt_click)
        self.loadButt.setIcon(QIcon('icons/load.png'))
        self.oButt = QPushButton(self.w)
        self.oButt.setText(f"About {App_Name}")
        self.oButt.move(350,0)
        self.oButt.resize(120,20)
        self.oButt.setIcon(QIcon('icons/info.png'))
        self.oButt.clicked.connect(self.AboutDialog)
        # text area
        self.TextEditor = QPlainTextEdit(self.w)
        self.TextEditor.resize(470,430)
        self.TextEditor.move(0,20)
        # load the file if exists
        if len(sys.argv) > 1:
            try:
                ltf = open(sys.argv[1], "r")
                self.TextEditor.clear()
                self.TextEditor.insertPlainText(ltf.read())
                ltf.close()
                self.LastFile = sys.argv[1]
            except:
                print("Could not load file")
        self.w.show()
    def openButt_click(self):
        try:
            self.getFileName = QFileDialog.getOpenFileName(self.w, "Open File", '.', "Text files (*.txt *.log *.md)")
            lf = open(self.getFileName[0], "r")
            self.TextEditor.clear()
            self.TextEditor.insertPlainText(lf.read())
            lf.close()
            self.LastFile = self.getFileName[0]
        except:
            print("Could not load file")
    def saveButt_click(self):
        try:
            print(self.LastFile)
            if self.LastFile == "None":
                self.getSaveName = QFileDialog.getSaveFileName(self.w, "Save File", '.', "Text files (*.txt *.log *.md)")
                sf = open(self.getSaveName[0], "w")
                sf.write(self.TextEditor.toPlainText())
                self.LastFile = self.getSaveName[0]
                sf.close()
            else:
                sf = open(self.LastFile, "w")
                sf.write(self.TextEditor.toPlainText())
                sf.close()
        except:
            print("Could not save file")

    def newButt_click(self):
        self.TextEditor.clear()
        self.LastFile = "None"

    def AboutDialog(self):
        self.About = QWidget()
        self.About.setWindowTitle(f"About {App_Name}")
        self.About.setGeometry(100,100,200,150)
        self.am = QLabel(self.About)
        self.am.setText(f"{App_Name} version {App_Version}.\nWritten in Python and Qt5")
        self.am.move(10,10)
        self.Logo = QLabel(self.About)
        self.Logo.setPixmap(QPixmap(f"icons/{App_Name_Small}_logo.png"))
        self.Logo.move(0,65)
        self.About.show()
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
